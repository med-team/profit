Source: profit
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: non-free/science
XS-Autobuild: no
Priority: optional
Build-Depends: debhelper (>= 10),
               libreadline-dev
Standards-Version: 4.1.2
Vcs-Browser: https://salsa.debian.org/med-team/profit
Vcs-Git: https://salsa.debian.org/med-team/profit.git
Homepage: http://www.bioinf.org.uk/software/profit/

Package: profit-3d
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         profit-data
Description: Protein structure alignment
 ProFit is designed to be the ultimate protein least squares fitting
 program. It has many features including flexible specification of
 fitting zones and atoms, calculation of RMS over different zones or
 atoms, RMS-by-residue calculation, on-line help facility, etc.
 .
 The binary is named profit-3D because of a name clash with an
 EMBOSS tool.

Package: profit
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         profit-3d
Conflicts: emboss
Description: Protein structure alignment
 ProFit is designed to be the ultimate protein least squares fitting
 program. It has many features including flexible specification of
 fitting zones and atoms, calculation of RMS over different zones or
 atoms, RMS-by-residue calculation, on-line help facility, etc.
 .
 A symbolic link is provided to have the binary name back to how
 it is historically correct.

Package: profit-data
Architecture: all
Recommends: profit
Description: Help and data files for Profit
 Nothing to be redistributed.
